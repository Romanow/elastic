package ru.romanow.elastic.common.model;

/**
 * Created by ronin on 09.04.17
 */
public enum Gender {
    MALE, FEMALE
}

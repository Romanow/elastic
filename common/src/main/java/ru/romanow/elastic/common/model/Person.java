package ru.romanow.elastic.common.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * Created by ronin on 09.04.17
 */
@Data
@Accessors(chain = true)
public class Person {
    private String firstName;
    private String lastName;
    private Gender gender;
    private Integer age;
    private List<Integer> marks;
}

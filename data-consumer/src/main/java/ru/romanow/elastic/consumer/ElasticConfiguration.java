package ru.romanow.elastic.consumer;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;

/**
 * Created by romanow on 11.04.17
 */
@Configuration
public class ElasticConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(ElasticConfiguration.class);

    private static final int ELASTIC_PORT = 9300;
    private static final String ELASTIC_HOST = "localhost";
    private static final String CLUSTER_NAME = "docker-cluster";

    @Bean
    public TransportClient transportClient() throws Exception {
        Settings settings =
                Settings.builder()
                        .put("cluster.name", CLUSTER_NAME)
                        .build();

        InetSocketTransportAddress address =
                new InetSocketTransportAddress(InetAddress.getByName(ELASTIC_HOST), ELASTIC_PORT);

        return new PreBuiltTransportClient(settings)
                .addTransportAddress(address);
    }
}

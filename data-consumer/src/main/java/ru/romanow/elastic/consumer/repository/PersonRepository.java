package ru.romanow.elastic.consumer.repository;

import ru.romanow.elastic.common.model.Person;

/**
 * Created by romanow on 11.04.17
 */
public interface PersonRepository {
    void save(Person person) throws Exception;
}

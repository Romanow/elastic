package ru.romanow.elastic.consumer.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.romanow.elastic.common.model.Person;

import java.util.Map;
import java.util.UUID;

/**
 * Created by romanow on 11.04.17
 */
@Repository
public class PersonRepositoryImpl
        implements PersonRepository {
    private static final Logger logger = LoggerFactory.getLogger(PersonRepository.class);

    private final TransportClient transportClient;

    @Autowired
    public PersonRepositoryImpl(TransportClient transportClient) {
        this.transportClient = transportClient;
    }

    @Override
    public void save(Person person) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = (Map<String, Object>)objectMapper.convertValue(person, Map.class);
        IndexRequest request = new IndexRequest("persons", "person")
                .source(map)
                .id(UUID.randomUUID().toString())
                .opType(IndexRequest.OpType.INDEX);
        IndexResponse responses = transportClient.index(request).get();
        logger.info("Send {} to elastic: {}", person, responses.status());
    }
}

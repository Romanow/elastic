package ru.romanow.elastic.consumer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.romanow.elastic.common.model.Person;
import ru.romanow.elastic.consumer.repository.PersonRepository;

/**
 * Created by ronin on 09.04.17
 */
@Service
public class MessageReceiverService {
    private static final Logger logger = LoggerFactory.getLogger(MessageReceiverService.class);

    private static final String QUEUE_NAME = "elastic-data";

    private final PersonRepository personRepository;

    @Autowired
    public MessageReceiverService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @RabbitListener(queues = QUEUE_NAME)
    public void receive(Person person) throws Exception {
        logger.info("Receive {}", person);

        personRepository.save(person);
    }
}

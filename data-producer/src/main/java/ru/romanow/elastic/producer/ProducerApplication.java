package ru.romanow.elastic.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchRepositoriesAutoConfiguration;
import ru.romanow.elastic.common.model.Person;
import ru.romanow.elastic.producer.services.DataGeneratorService;
import ru.romanow.elastic.producer.services.MessageSenderService;

/**
 * Created by ronin on 09.04.17
 */
@SpringBootApplication(exclude = {
        ElasticsearchAutoConfiguration.class,
        ElasticsearchDataAutoConfiguration.class,
        ElasticsearchRepositoriesAutoConfiguration.class
})
public class ProducerApplication
        implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(ProducerApplication.class);

    private static final int ITEM_COUNT = 20;

    @Autowired
    private MessageSenderService messageSenderService;

    @Autowired
    private DataGeneratorService dataGeneratorService;

    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        for (int i = 0; i < ITEM_COUNT; i++) {
            Person person = dataGeneratorService.generatePerson();
            logger.info("Sending {} to consumer", person);
            messageSenderService.send(person);
        }
        logger.info("Successfully sending {} items", ITEM_COUNT);
    }
}

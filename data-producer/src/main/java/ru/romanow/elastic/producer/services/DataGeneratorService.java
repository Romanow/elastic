package ru.romanow.elastic.producer.services;

import ru.romanow.elastic.common.model.Person;

/**
 * Created by ronin on 09.04.17
 */
public interface DataGeneratorService {
    Person generatePerson();
}

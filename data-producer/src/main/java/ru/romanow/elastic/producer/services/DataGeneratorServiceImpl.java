package ru.romanow.elastic.producer.services;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import ru.romanow.elastic.common.model.Gender;
import ru.romanow.elastic.common.model.Person;

import java.util.List;

import static org.apache.commons.lang3.RandomUtils.nextInt;

/**
 * Created by ronin on 09.04.17
 */
@Service
public class DataGeneratorServiceImpl
        implements DataGeneratorService {
    private static final String[] NAMES = {
            "Alex", "Max", "Mike", "Bob", "Charlie", "Duke", "Lord"
    };

    private static final String[] SURNAMES = {
            "Romanow", "Alexadrov", "Shepilin", "Kitten", "Brown",
            "Elfel", "Dubinin", "Stroganov", "Bolotin", "Kretov"
    };

    @Override
    public Person generatePerson() {
        return new Person()
                .setAge(nextInt(10, 51))
                .setGender(nextBoolean() ? Gender.MALE : Gender.FEMALE)
                .setFirstName(NAMES[getNamePosition()])
                .setLastName(SURNAMES[getSurnamePosition()])
                .setMarks(generateMarks());
    }

    private int getNamePosition() {
        return nextInt(0, NAMES.length);
    }

    private int getSurnamePosition() {
        return nextInt(0, SURNAMES.length);
    }

    private boolean nextBoolean() {
        return nextInt(0, 2) == 0;
    }

    private List<Integer> generateMarks() {
        return Lists.newArrayList(nextInt(2, 6), nextInt(2, 6), nextInt(2, 6));
    }
}

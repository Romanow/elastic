package ru.romanow.elastic.producer.services;

import com.google.gson.Gson;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.romanow.elastic.common.model.Person;

import static ru.romanow.elastic.producer.RabbitConfiguration.QUEUE_NAME;

/**
 * Created by ronin on 09.04.17
 */
@Service
public class MessageSenderServiceImpl
        implements MessageSenderService {

    private final Gson gson;
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public MessageSenderServiceImpl(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        this.gson = new Gson();
    }

    @Override
    public void send(Person person) {
        rabbitTemplate.convertAndSend(QUEUE_NAME, person);
    }
}
